package br.com.portal.fachada.rest.paginacao;

import br.com.portal.modelo.dto.DataDTO;

import java.util.Collection;

public class Pagina<E extends DataDTO> {

	private Collection<E> content;

    private long totalElements;
    
	public Collection<E> getContent() {
		return content;
	}

	public void setContent(Collection<E> content) {
		this.content = content;
	}

	public long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}

}