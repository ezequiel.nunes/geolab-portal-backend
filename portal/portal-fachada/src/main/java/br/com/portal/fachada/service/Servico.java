package br.com.portal.fachada.service;

import br.com.portal.fachada.dao.DAO;
import br.com.portal.fachada.rest.paginacao.Pagina;
import br.com.portal.fachada.rest.paginacao.Paginacao;
import br.com.portal.modelo.base.IEntidade;
import br.com.portal.modelo.dto.DataDTO;
import org.springframework.data.domain.Example;

import java.io.Serializable;
import java.util.List;

public interface Servico<ID extends Serializable, E extends IEntidade> {

    E get(ID id);

    E salvar(E entidade);

    E alterar(E entidade);

    void salvar(List<E> lista);
    
    void excluir(E entidade);
    
    void excluirPorId(ID id);

    List<E>  listar();
    
    Pagina<? extends DataDTO> listarPaginadoDTO(final Paginacao<E> paginacao);
    
    Example<E> obterRestricoes(final Paginacao<E> paginacao);
    
    Long count();
    
    DAO<ID, E> getDAO();
    
    Class<? extends DataDTO> getDTO();
}