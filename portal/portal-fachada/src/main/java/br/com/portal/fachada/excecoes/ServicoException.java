package br.com.portal.fachada.excecoes;

import java.text.MessageFormat;

public class ServicoException extends EndpointException {

	private static final long serialVersionUID = 1L;

	public ServicoException(final String mensagem, final Object... argumentos) {
		
        super(mensagem, argumentos);
    }

    public String buildMensagem() {

        return MessageFormat.format(this.getMessage(), this.getArgumentos());
    }
    
}