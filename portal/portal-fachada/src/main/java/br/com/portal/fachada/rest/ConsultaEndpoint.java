package br.com.portal.fachada.rest;

import br.com.portal.fachada.dto.SelectItemDTO;
import br.com.portal.fachada.excecoes.ServicoException;
import br.com.portal.fachada.rest.paginacao.Pagina;
import br.com.portal.fachada.rest.paginacao.Paginacao;
import br.com.portal.fachada.service.Servico;
import br.com.portal.modelo.base.IEntidade;
import br.com.portal.modelo.dto.DataDTO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class ConsultaEndpoint<E extends IEntidade> extends BaseEndpoint {

    private static final long serialVersionUID = 1L;

    protected abstract <T extends Serializable> Servico<T, E> getServico();

    @POST
    @Path("/listar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Pagina<? extends DataDTO> listar(final Paginacao<E> paginacao) {
        return this.getServico().listarPaginadoDTO(paginacao);
    }

    @GET
    @Path("/listarItem")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<E>> listarItem() {
        return this.obterListaDeSelectItem(this.getServico().listar());
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<E> listar() {
        return this.getServico().listar();
    }

    @GET
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public E dtoGet(@PathParam("id") Long id) {
        return this.getServico().get(id);
    }

    @POST
    @Path("/exportar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<byte[]> exportar() throws Exception {

        try {

            final String fileName = String.format("relatorio_%s.xlsx", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy_HHmmss")));

            final HttpHeaders headers = new HttpHeaders();

            headers.setContentDispositionFormData(fileName, fileName);

            return new ResponseEntity<byte[]>(exportarParaExcel(), headers, HttpStatus.OK);

        } catch (final Exception e) {

            throw e;

        }

    }

    protected List<SelectItemDTO<E>> obterListaDeSelectItem(final Collection<E> entidades) {

        final List<SelectItemDTO<E>> collect = new ArrayList<>();

        collect.add(new SelectItemDTO<E>("Selecione", null));

        collect.addAll(entidades

                .stream().map(f -> new SelectItemDTO<E>(this.toStringItemSelectIem().apply(f), f))

                .collect(Collectors.toList()));

        return collect;

    }

    protected Function<? super E, ? extends String> toStringItemSelectIem() {

        throw new ServicoException("Método não implementado.");

    }

    protected byte[] exportarParaExcel() throws Exception {

        throw new ServicoException("Método não implementado.");

    }

}