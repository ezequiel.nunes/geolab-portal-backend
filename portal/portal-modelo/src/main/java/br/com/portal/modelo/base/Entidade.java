package br.com.portal.modelo.base;

import br.com.portal.modelo.Enumerator.StatusEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public class Entidade implements IEntidade {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "dt_cadastro")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    @NotNull(message = "MSGE006")
    private StatusEnum status;

    @PrePersist
    public void preSalvar() {
        if (dataCadastro == null) {
            dataCadastro = new Date();
        }

    }


}
