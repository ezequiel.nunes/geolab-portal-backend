package br.com.portal.modelo.Enumerator;

import lombok.Getter;

@Getter
public enum StatusIntegracaoEnum {

    OK,
    NOK;

    @Override
    public String toString() {
        return this.name();
    }
}
