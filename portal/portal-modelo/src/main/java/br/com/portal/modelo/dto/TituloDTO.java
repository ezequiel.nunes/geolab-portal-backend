package br.com.portal.modelo.dto;

import br.com.portal.modelo.Enumerator.StatusTituloEnum;
import br.com.portal.modelo.Titulo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TituloDTO extends DataDTO {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long idCliente;

    private String numeroNotaFiscal;

    private Date dataEmissao;

    private Date dataVencimento;

    private BigDecimal valorNominal;

    private String statusTitulo;

    private String numeroDocumento;

    private String parcela;

    public String getStatusTitulo(){
        return atribuirStatus();
    }

    private String atribuirStatus() {
        Date hoje = new Date();
        if(Objects.nonNull(dataVencimento)&& dataVencimento.before(hoje)){
           this.statusTitulo = StatusTituloEnum.VENCIDO.getDescricao();
        }else{
            this.statusTitulo = StatusTituloEnum.EM_ABERTO.getDescricao();
        }
        return this.statusTitulo;
    }

    public TituloDTO(final Titulo titulo) {
        setId(titulo.getId());
        setIdCliente(titulo.getCliente().getId());
        setNumeroNotaFiscal(titulo.getNumeroNotaFiscal());
        setDataEmissao(titulo.getDataEmissao());
        setDataVencimento(titulo.getDataVencimento());
        setValorNominal(titulo.getValorNominal());
        setDataCadastro(titulo.getDataCadastro());
        setNumeroDocumento(titulo.getNumeroDocumento());
        setParcela(titulo.getParcela());
        setStatusTitulo(this.atribuirStatus());
    }
}
