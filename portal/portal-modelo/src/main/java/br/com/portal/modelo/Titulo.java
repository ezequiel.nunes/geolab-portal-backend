package br.com.portal.modelo;

import br.com.portal.modelo.Enumerator.StatusTituloEnum;
import br.com.portal.modelo.base.IEntidade;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "tb_titulo")
public class Titulo implements IEntidade {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    @NotNull(message = "titulos.cliente.validacao")
    private UsuarioCliente cliente;

    @Column(name = "nr_nota_fiscal", nullable = false)
    @NotNull(message = "titulos.numeroNotaFiscal.validacao")
    private String numeroNotaFiscal;

    @Column(name = "dt_emissao", nullable = false)
    @NotNull(message = "titulos.dataEmissao.validacao")
    @Temporal(value = TemporalType.DATE)
    private Date dataEmissao;

    @Column(name = "dt_vencimento", nullable = false)
    @NotNull(message = "titulos.dataVencimento.validacao")
    @Temporal(value = TemporalType.DATE)
    private Date dataVencimento;

    @Column(name = "vlr_nominal", nullable = false)
    @NotNull(message = "titulos.valorNominal.validacao")
    private BigDecimal valorNominal;

    @Column(name = "dt_cadastro")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @Column(name = "nr_documento")
    private String numeroDocumento;

    @Column(name = "parcela")
    private String parcela;

    @Transient
    private transient Date emissaoInicial;

    @Transient
    private transient Date emissaoFinal;

    @Transient
    private transient Date vencimentoInicial;

    @Transient
    private transient Date vencimentoFinal;

    @Transient
    private transient BigDecimal valorInicial;

    @Transient
    private transient BigDecimal valorFinal;

    @Transient
    private transient String numeroNotaFiscalFiltro;

    @Transient
    private transient Long idClienteFiltro;

    @Transient
    private transient StatusTituloEnum statusTitulo;


    @PrePersist
    public void preSalvar() {
        if (dataCadastro == null) {
            dataCadastro = new Date();
        }
    }
}
