package br.com.portal.modelo.dto.integracao;

import br.com.portal.modelo.Enumerator.StatusIntegracaoEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "RETORNO")
public class MensagemIntegracaoDTO {

    @JsonProperty("CODIGO")
    private String codigo;

    @JsonProperty("STATUS")
    private StatusIntegracaoEnum status;

    @JsonProperty("MENSAGEM")
    private String mensagem;

}
