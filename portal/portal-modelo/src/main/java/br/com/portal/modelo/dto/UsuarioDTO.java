package br.com.portal.modelo.dto;

import br.com.portal.modelo.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioDTO extends DataDTO {

	private String login;

	private String nome;

	private String email;

	private boolean administrador;

    private boolean alterarSenha;

	public UsuarioDTO(final Usuario usuario) {
		setId(usuario.getId());
		setLogin(usuario.getLogin());
		setNome(usuario.getNome());
		setEmail(usuario.getEmail());
		setAdministrador(usuario.isAdministrador());
		setAlterarSenha(usuario.isAlterarSenha());
		setStatus(usuario.getStatus().getDescricao());
	}

}
