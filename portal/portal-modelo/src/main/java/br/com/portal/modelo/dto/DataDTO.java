package br.com.portal.modelo.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public abstract class DataDTO implements Serializable {
	
	private Long id;
	
	private String status;

	private Date dataCadastro;
	
}
