package br.com.portal.modelo;

import br.com.portal.modelo.base.Entidade;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_usuario")
public class Usuario extends Entidade {

    private static final long serialVersionUID = 1L;

    @Column(name = "nome")
    @NotNull(message = "MSGE006")
    private String nome;

    @Column(name = "login", unique = true)
    @NotNull(message = "MSGE006")
    private String login;

    @JsonIgnore
    @Column(name = "senha")
    @NotNull(message = "MSGE006")
    private String senha;

    @Column(name = "email", unique = true)
    @NotNull(message = "MSGE006")
    @Pattern(regexp ="^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.(?:[a-zA-Z]{2,6})$", message="MSGE003")
    private String email;

    @Column(name = "administrador")
    private boolean administrador;

    @Column(name = "alterar_senha")
    private boolean alterarSenha;

    @Transient
    private transient String pesquisa;

}
