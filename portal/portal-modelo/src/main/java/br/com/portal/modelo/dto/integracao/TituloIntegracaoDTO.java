package br.com.portal.modelo.dto.integracao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "TITULO")
public class TituloIntegracaoDTO {

    @JsonProperty("COD_CLIENTE")
    private String codigoCliente;

    @JsonProperty("NR_NOTA_FISCAL")
    private String numeroNotaFiscal;

    @JsonProperty("NR_DOCUMENTO")
    private String numeroDocumento;

    @JsonProperty("PARCELA")
    private String parcela;

    @JsonProperty("DATA_EMISSAO")
    private Date dataEmissao;

    @JsonProperty("DATA_VENCIMENTO")
    private Date dataVencimento;

    @JsonProperty("VALOR_NOMINAL")
    private BigDecimal valorNominal;

}
