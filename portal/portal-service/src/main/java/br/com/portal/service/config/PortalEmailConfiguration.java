package br.com.portal.service.config;

import br.com.portal.modelo.dto.ConfiguracaoDTO;
import br.com.portal.service.ConfiguracaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.sql.DataSource;
import java.util.Objects;
import java.util.Properties;

@Configuration
public class PortalEmailConfiguration {

    @Autowired
    public ConfiguracaoService service;

    @Autowired
    private Environment env;

    @Bean
    public JavaMailSender getJavaMailSender() {

        ConfiguracaoDTO configuracao = ConfiguracaoDTO.getInstance(this.service.listar());
        if (Objects.nonNull(configuracao) ) {
           return configuracaoBaseDados(configuracao);
        }else{
            return null;
        }
    }

    private JavaMailSender propoertyConfiguration() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(env.getProperty("spring.mail.host"));
        mailSender.setPort(Integer.parseInt(env.getProperty("spring.mail.port")));
        mailSender.setUsername(env.getProperty("spring.mail.username"));
        mailSender.setPassword(env.getProperty("spring.mail.password="));

        Properties props = mailSender.getJavaMailProperties();
        // props.put("mail.transport.protocol", env.getProperty(""));
        props.put("mail.smtp.auth", env.getProperty("spring.mail.properties.mail.smtp.auth"));
        props.put("mail.smtp.starttls.enable", env.getProperty("spring.mail.properties.mail.smtp.starttls.enable"));
        props.put("mail.smtp.starttls.required", env.getProperty("spring.mail.properties.mail.smtp.starttls.required"));
        props.put("mail.smtp.ssl.trust", env.getProperty("spring.mail.properties.mail.smtp.ssl.trust"));
        return mailSender;
    }

    private JavaMailSender configuracaoBaseDados(ConfiguracaoDTO configuracao) {
            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
            mailSender.setHost(configuracao.getEmailServidor());
            mailSender.setPort(Integer.parseInt(configuracao.getEmailPorta()));
            mailSender.setUsername(configuracao.getEmailEmail());
            mailSender.setPassword(configuracao.getEmailSenha());

            Properties props = mailSender.getJavaMailProperties();
            //props.put("mail.transport.protocol", configuracao.getEmailProtocolo());
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.starttls.required", "true");
            props.put("mail.smtp.ssl.trust", configuracao.getEmailServidor());
            return mailSender;
    }

}