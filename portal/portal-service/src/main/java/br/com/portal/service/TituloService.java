package br.com.portal.service;


import br.com.portal.fachada.service.Servico;
import br.com.portal.modelo.Titulo;
import br.com.portal.modelo.dto.integracao.MensagemIntegracaoDTO;
import br.com.portal.modelo.dto.integracao.TituloIntegracaoDTO;

import java.util.List;

public interface TituloService extends Servico<Long, Titulo> {

    List<MensagemIntegracaoDTO> salvarTitulos(List<TituloIntegracaoDTO> titulos);
}
