package br.com.portal.service;

import br.com.portal.fachada.service.Servico;
import br.com.portal.modelo.Configuracao;

public interface ConfiguracaoService extends Servico<Long, Configuracao> {

}
