package br.com.portal.service;


import br.com.portal.fachada.service.Servico;
import br.com.portal.modelo.Usuario;
import br.com.portal.modelo.dto.AlterarPerfilDTO;

public interface UsuarioService extends Servico<Long, Usuario> {

	Usuario autenticarUsuario(final String login, final String senha);

	String obterEmailBasePorLogin(String login);

	void recuperarSenha(String login, String email);

	void alterarPerfil(AlterarPerfilDTO entidade);
}
