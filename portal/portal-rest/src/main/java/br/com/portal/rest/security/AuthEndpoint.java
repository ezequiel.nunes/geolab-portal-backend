package br.com.portal.rest.security;

import br.com.portal.fachada.excecoes.ServicoException;
import br.com.portal.fachada.rest.BaseEndpoint;
import br.com.portal.modelo.Usuario;
import br.com.portal.modelo.UsuarioCliente;
import br.com.portal.modelo.dto.UsuarioDTO;
import br.com.portal.rest.contexto.ContextoManager;
import br.com.portal.service.UsuarioClienteService;
import br.com.portal.service.UsuarioService;
import br.com.portal.util.bundle.MessageSupport;
import br.com.portal.util.encrypt.UtilEncoder;
import br.com.portal.util.rest.MapBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@PermitAll
@Component
@Path("auth")
public class AuthEndpoint extends BaseEndpoint {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ContextoManager contextoManager;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private UsuarioClienteService usuarioClienteService;

	private static final String REQUEST_PARAMETER_PASSWORD = "password";

	protected static final String REQUEST_PARAMETER_USERNAME = "username";

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddhhmmss");


	@POST
	public Response auth() {

		final String usuario = request.getParameter(REQUEST_PARAMETER_USERNAME);

		final String senha = request.getParameter(REQUEST_PARAMETER_PASSWORD);

		final Usuario usuarioLogado = this.processLogin(usuario, senha);

		final String token = UtilEncoder.get().encrypt(String.format("%s:%s:%s", usuario, senha, this.now()));

		this.registrarToken(usuarioLogado, token);

		if(usuarioLogado instanceof UsuarioCliente){

			UsuarioCliente cliente = (UsuarioCliente) usuarioLogado;

			cliente.setDataUltimoAcesso(new Date());

			usuarioClienteService.alterar(cliente);
		}

		return this.createResponseAuth(token, usuarioLogado);
	}

	@GET
	@Path("logout")
	public Response logout() {

		if (this.request.getHeader(BaseEndpoint.AUTHORIZATION_PROPERTY) != null) {

			this.processLogout(this.request.getHeader(BaseEndpoint.AUTHORIZATION_PROPERTY));
		}

		return Response.noContent().build();
	}

	protected Usuario processLogin(String login, String senha) throws NotAuthorizedException {

		if (StringUtils.isBlank(login) || StringUtils.isBlank(senha)) {

			throw new ServicoException(MessageSupport.getMessage("MSG006"));

		}

		final Usuario usuario = usuarioService.autenticarUsuario(login, senha);

		if (usuario == null) {

			throw new ServicoException(MessageSupport.getMessage("MSGE001"));
		}

		return usuario;

	}

	protected Response createResponseAuth(String token, Usuario usuario) {

		final Response response = Response.ok(MapBuilder.create("token", token)

				.map("usuario", new UsuarioDTO(usuario))

				.build(), MediaType.APPLICATION_JSON).build();

		return response;
	}

	protected void processLogout(String token) {

		this.contextoManager.remover(token);
	}

	protected void registrarToken(Usuario usuarioEndpoint, String token) {

		this.contextoManager.armazenar(token, usuarioEndpoint);
	}

	protected String now() {

		return DATE_FORMAT.format(new Date());
	}

}
