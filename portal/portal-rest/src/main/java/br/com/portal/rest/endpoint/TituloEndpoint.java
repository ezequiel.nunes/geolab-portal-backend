package br.com.portal.rest.endpoint;

import br.com.portal.fachada.excecoes.ServicoException;
import br.com.portal.fachada.rest.ManutencaoEndpoint;
import br.com.portal.modelo.Titulo;
import br.com.portal.modelo.dto.integracao.MensagemIntegracaoDTO;
import br.com.portal.modelo.dto.integracao.TituloIntegracaoDTO;
import br.com.portal.service.TituloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@Component
@Path("titulo")
public class TituloEndpoint extends ManutencaoEndpoint<Titulo> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private TituloService service;

    @POST
    @PermitAll
    @Path("salvarTitulos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<MensagemIntegracaoDTO> salvarTitulos(List<TituloIntegracaoDTO> titulos){
        return service.salvarTitulos(titulos);
    }

    @POST
    @Path("/downloadTituloXLS/{idTitulo}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response downloadTituloXLS(@PathParam("idTitulo") Long idTitulo) {
        throw new ServicoException("Operação indisponível no momento");
    }

    @POST
    @Path("/downloadTituloPDF/{idTitulo}")
    @Produces("application/pdf")
    public Response downloadTituloPDF(@PathParam("idTitulo") Long idTitulo) {
        throw new ServicoException("Operação indisponível no momento");
    }

    @POST
    @Path("/downloadTituloBoleto/{idTitulo}")
    @Produces("application/pdf")
    public Response downloadTituloBoleto(@PathParam("idTitulo") Long idTitulo) {
        throw new ServicoException("Operação indisponível no momento");
    }

    @POST
    @Path("/downloadTituloBarras/{idTitulo}")
    @Produces("application/pdf")
    public Response downloadTituloBarras(@PathParam("idTitulo") Long idTitulo) {
        throw new ServicoException("Operação indisponível no momento");
    }

    private ByteArrayOutputStream getDocumentoPDF(String path) throws IOException {

        byte[] buffer = new byte[4096];

        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream(new FileInputStream(path));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int bytes = 0;

        while ((bytes = bis.read(buffer, 0, buffer.length)) > 0) {

            baos.write(buffer, 0, bytes);

        }

        baos.close();

        bis.close();

        return baos;
    }

    @Override
    protected Function<? super Titulo, ? extends String> toStringItemSelectIem() {
        return u -> u.getNumeroNotaFiscal();
    }

    @Override
    protected TituloService getServico() {
        return service;
    }
}
        /*ByteArrayOutputStream byteArrayOutputStream = null;
        try {
            byteArrayOutputStream = getDocumentoPDF("C:\\temp\\geolab\\sample.xls");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Titulo titulo = tituloService.get(idTitulo);
        Response.ResponseBuilder response = Response.ok(byteArrayOutputStream.toByteArray());
        response.header("Content-Disposition", "attachment; filename=" + titulo.getNumeroNotaFiscal() + ".xls");
        response.header("Accept", MediaType.APPLICATION_OCTET_STREAM);
        return response.build();*/


        /*ByteArrayOutputStream byteArrayOutputStream = null;
        try {
            byteArrayOutputStream = getDocumentoPDF("C:\\temp\\geolab\\sample.pdf");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Titulo titulo = tituloService.get(idTitulo);
        Response.ResponseBuilder response = Response.ok(byteArrayOutputStream.toByteArray());
        response.header("Content-Disposition", "attachment; filename=" + titulo.getNumeroNotaFiscal() + ".pdf");
        return response.build();*/