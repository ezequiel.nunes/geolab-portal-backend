package br.com.portal.rest.endpoint;

import br.com.portal.fachada.rest.ManutencaoEndpoint;
import br.com.portal.modelo.Usuario;
import br.com.portal.service.UsuarioService;
import br.com.portal.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.function.Function;

@Component
@PermitAll
@Path("recuperar-senha")
public class RecuperarSenhaEndpoint extends ManutencaoEndpoint<Usuario> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private UsuarioService usuarioService;

    @POST
    @Consumes({MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    @Path("obterBaseEmailPorLogin")
    public Response obterBaseEmailPorLogin(final Usuario entidade) {

        final String mailBase = usuarioService.obterEmailBasePorLogin(entidade.getLogin());

        return Response.ok(MapBuilder.create("email", mailBase).build()).build();

    }

	@POST
	@Path("/{login}/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response recuperarSenha(@PathParam("login") String login, @PathParam("email") String email) {

    	usuarioService.recuperarSenha(login,email);

		return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSGS002")).build()).build();
	}


	@Override
	protected Function<? super Usuario, ? extends String> toStringItemSelectIem() {

		return u -> u.getNome();

	}

	@Override
	protected UsuarioService getServico() {
		return usuarioService;
	}
}
