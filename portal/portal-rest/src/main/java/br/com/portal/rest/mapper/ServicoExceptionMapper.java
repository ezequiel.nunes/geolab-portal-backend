package  br.com.portal.rest.mapper;

import br.com.portal.fachada.excecoes.ServicoException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ServicoExceptionMapper implements ExceptionMapper<ServicoException> {

    @Override
    public Response toResponse(ServicoException exception) {

        return MapperUtils.create(Response.Status.fromStatusCode(412), exception.buildMensagem());
    }
}
