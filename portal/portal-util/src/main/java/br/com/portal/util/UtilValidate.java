package br.com.portal.util;

import java.util.regex.Pattern;

public class UtilValidate {

    public static final String CONTEM_LETRA_NUMERO = "^(?=.*[a-zA-Z])(?=.*[0-9])";
    public static final String CONTEM_CHARACTER_ESPECIAL = "[^a-zA-Z0-9]";

    public static boolean isSenhaValida(String senha) {

        Pattern p1 = Pattern.compile(CONTEM_LETRA_NUMERO);

        boolean contemNumeroELetra = p1.matcher(senha).find();

        Pattern p2 = Pattern.compile(CONTEM_CHARACTER_ESPECIAL);

        boolean contemCarecterEspecial = p2.matcher(senha).find();

        return contemNumeroELetra && contemCarecterEspecial;
    }
}
