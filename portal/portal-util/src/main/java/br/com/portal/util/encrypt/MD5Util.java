package br.com.portal.util.encrypt;

import java.security.MessageDigest;
import java.util.Random;

public class MD5Util {

	public static String sha256(String value) {

		try {

			MessageDigest digest = MessageDigest.getInstance("SHA-256");

			byte[] hash = digest.digest(value.getBytes("UTF-8"));

			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) {

				String hex = Integer.toHexString(0xff & hash[i]);

				if (hex.length() == 1) hexString.append('0');

				hexString.append(hex);
			}

			return hexString.toString();

		} catch (Exception ex) {

			throw new RuntimeException(ex);
		}
	}

	public static String cript(final String str) {

		try {

			if (str == null || str.length() == 0) {
				throw new IllegalArgumentException("String to encript cannot be null or zero length");
			}

			MessageDigest digester = MessageDigest.getInstance("MD5");

			digester.update(str.getBytes());

			final byte[] hash = digester.digest();

			final StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) {

				if (( 0xff & hash[i] ) < 0x10) {

					hexString.append("0" + Integer.toHexString(( 0xFF & hash[i] )));

				} else {

					hexString.append(Integer.toHexString(0xFF & hash[i]));

				}

			}

			return hexString.toString();

		} catch (Exception e) {
			
			return "0000001";

		}

	}

	public static String geraNovaSenha() {
		char[] ALL_CHARS = new char[62];
		for (int i = 48, j = 0; i < 123; i++) {
			if (Character.isLetterOrDigit(i)) {
				ALL_CHARS[j] = (char) i;
				j++;
			}
		}

		final char[] result = new char[8];
		for (int i = 0; i < 8; i++) {
			result[i] = ALL_CHARS[new Random().nextInt(ALL_CHARS.length)];
		}

		String novaSenha = new String(result);

		System.out.println("Nova Senha: " + novaSenha);

		return novaSenha;
	}

}
