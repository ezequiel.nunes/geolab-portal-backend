package br.com.portal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = PortalApplication.BASE_COMPONENT)
public class PortalApplication {

    public static final String BASE_COMPONENT = "br.com.portal";

    public static void main(String[] args) {

        SpringApplication.run(PortalApplication.class, args);

    }
}
