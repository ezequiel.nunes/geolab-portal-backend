package br.com.portal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class ConfigurationFilter {
	
	@Autowired
	private DataSource dataSource;

    @Bean
    public FilterRegistrationBean<CORSFilter> registrarCORSFilter() {
    	
        final FilterRegistrationBean<CORSFilter> registration = new FilterRegistrationBean<>();

        registration.setFilter(new CORSFilter());
        
        registration.addUrlPatterns("/*");
        
        registration.setName("CORSFilter");
        
        registration.setOrder(1);

        return registration;
    }

}
