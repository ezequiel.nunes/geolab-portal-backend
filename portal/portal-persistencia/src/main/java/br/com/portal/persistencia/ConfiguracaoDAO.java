package br.com.portal.persistencia;

import br.com.portal.fachada.dao.DAO;
import br.com.portal.modelo.Configuracao;
import br.com.portal.modelo.Usuario;

public interface ConfiguracaoDAO extends DAO<Long, Configuracao> {

}
