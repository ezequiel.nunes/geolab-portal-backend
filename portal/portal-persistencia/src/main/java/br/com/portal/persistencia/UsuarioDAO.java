package br.com.portal.persistencia;

import br.com.portal.fachada.dao.DAO;
import br.com.portal.modelo.Enumerator.StatusEnum;
import br.com.portal.modelo.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UsuarioDAO extends DAO<Long, Usuario> {

	Usuario findByLoginAndSenhaAndStatus(String login, String senha, StatusEnum status);

	Usuario findByLoginAndStatus(String login, StatusEnum status);

	Usuario findByLogin(String login);

	Usuario findByEmail(String email);

	Page<Usuario> findByAdministradorTrueOrderByIdDesc(Pageable pageable);

}
